package com.rnp.simplenavigation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.rnp.simplenavigation.bottomnav1.Nav1HostFragment
import com.rnp.simplenavigation.bottomnav2.Nav2HostFragment
import com.rnp.simplenavigation.bottomnav3.Nav3HostFragment
import com.rnp.simplenavigation.navigation.BackPressConsumer
import com.rnp.simplenavigation.navigation.FragmentStateManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    companion object {
        const val BOTTOM_TAB_NAV1 = 0
        const val BOTTOM_TAB_NAV2 = 1
        const val BOTTOM_TAB_NAV3 = 2

        const val BOTTOM_TAB_NAV1_TAG = "Nav1HostFragment"
        const val BOTTOM_TAB_NAV2_TAG = "Nav2HostFragment"
        const val BOTTOM_TAB_NAV3_TAG = "Nav3HostFragment"
    }

    private lateinit var fragmentStateManager: FragmentStateManager

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation1 -> {

                fragmentStateManager.changeFragment(BOTTOM_TAB_NAV1)

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation2 -> {

                fragmentStateManager.changeFragment(BOTTOM_TAB_NAV2)

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation3 -> {

                fragmentStateManager.changeFragment(BOTTOM_TAB_NAV3)

                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    private val mOnNavigationItemReselectedListener = BottomNavigationView.OnNavigationItemReselectedListener { item ->
        val position = getNavPositionFromMenuItem(item)
        if (position != -1) {
            fragmentStateManager.removeFragment(position)
            fragmentStateManager.changeFragment(position)
        }
    }

    fun getNavPositionFromMenuItem(menuItem: MenuItem): Int {
        return when (menuItem.getItemId()) {
            R.id.navigation1 -> BOTTOM_TAB_NAV1
            R.id.navigation2 -> BOTTOM_TAB_NAV2
            R.id.navigation3 -> BOTTOM_TAB_NAV3
            else -> -1
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        fragmentStateManager = object : FragmentStateManager(fragmentContainer, supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    BOTTOM_TAB_NAV1 -> Nav1HostFragment()
                    BOTTOM_TAB_NAV2 -> Nav2HostFragment()
                    BOTTOM_TAB_NAV3 -> Nav3HostFragment()
                    else -> Nav1HostFragment()
                }
            }

            override fun getTag(position: Int): String {
                return when (position) {
                    BOTTOM_TAB_NAV1 -> BOTTOM_TAB_NAV1_TAG
                    BOTTOM_TAB_NAV2 -> BOTTOM_TAB_NAV2_TAG
                    BOTTOM_TAB_NAV3 -> BOTTOM_TAB_NAV3_TAG
                    else -> BOTTOM_TAB_NAV1_TAG
                }
            }
        }

        if (savedInstanceState == null) {
            fragmentStateManager.changeFragment(0)
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        // topWaypointsNavigation.setOnNavigationItemReselectedListener(mOnNavigationItemReselectedListener)
    }

    override fun onBackPressed() {
        val navHostFragment = supportFragmentManager.findFragmentByTag(getSelectedHostFragmentTag())
        if (navHostFragment != null && navHostFragment is BackPressConsumer) {
            if (navHostFragment.onBackPress()) {
                return
            }
        }
        super.onBackPressed()
    }

    private fun getSelectedHostFragmentTag(): String {

        if (navigation.selectedItemId == R.id.navigation1) {
            return BOTTOM_TAB_NAV1_TAG
        }

        if (navigation.selectedItemId == R.id.navigation2) {
            return BOTTOM_TAB_NAV2_TAG
        }

        if (navigation.selectedItemId == R.id.navigation3) {
            return BOTTOM_TAB_NAV3_TAG
        }

        return BOTTOM_TAB_NAV1_TAG
    }

}
