package com.rnp.simplenavigation.viewpager

import android.os.Bundle
import com.rnp.simplenavigation.samplehost.ChildHostFragment


class Test1HostFragment : ChildHostFragment() {
    companion object {
        fun newInstance(tabIndex: Int): Test1HostFragment {

            val bundle = Bundle()
            bundle.putInt("TAB_INDEX", tabIndex)

            val fragment = Test1HostFragment()
            fragment.arguments = bundle

            return fragment
        }
    }
}
