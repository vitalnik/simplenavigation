package com.rnp.simplenavigation.viewpager

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.rnp.simplenavigation.R
import com.rnp.simplenavigation.navigation.BackPressConsumer
import kotlinx.android.synthetic.main.fragment_nav1_viewpager.*


class ViewPagerFragment : Fragment(), BackPressConsumer {

    companion object {

        const val TAB_1 = 0
        const val TAB_2 = 1
        const val TAB_3 = 2

        fun newInstance(): ViewPagerFragment {
            return ViewPagerFragment()
        }

    }

    private var selectedTab: Int = 0

    private lateinit var viewPagerAdapter: ViewPagerAdapter

    inner class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                TAB_1 -> return Test1HostFragment.newInstance(TAB_1)
                TAB_2 -> return Test2HostFragment.newInstance(TAB_2)
                TAB_3 -> return Test3HostFragment.newInstance(TAB_3)
            }
            return null
        }

        override fun getCount(): Int {
            return 3
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                TAB_1 -> return "Tab 1"
                TAB_2 -> return "Tab 2"
                TAB_3 -> return "Tab 3"
            }
            return ""
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {

        Log.d("TAG", "!!! ViewPagerFragment onSaveInstanceState " + viewPager)

        outState.putInt("SELECTED_TAB_INDEX", viewPager?.currentItem ?: 0)

        super.onSaveInstanceState(outState)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        Log.d("TAG", "!!! ViewPagerFragment onAttach")
    }

    override fun onDetach() {
        super.onDetach()

        Log.d("TAG", "!!! ViewPagerFragment onDetach")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_nav1_viewpager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("TAG", "!!! ViewPagerFragment onViewCreated")

        setupToolbar()

        viewPagerAdapter = ViewPagerAdapter(childFragmentManager)

        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = 2
        tabLayout.setupWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                selectedTab = position
                updateTabTitle(position)
            }
        })

        Handler().postDelayed( {
            updateTabTitle(selectedTab)
        }, 150)
    }

    private fun updateTabTitle(position: Int) {

        val fragments = childFragmentManager.fragments
        for (fragment in fragments) {
            if (fragment is Test1HostFragment && position == TAB_1) {
                fragment.updateTitle(fragment.getCurrentTitle())
                return
            }
            if (fragment is Test2HostFragment && position == TAB_2) {
                fragment.updateTitle(fragment.getCurrentTitle())
                return
            }
            if (fragment is Test3HostFragment && position == TAB_3) {
                fragment.updateTitle(fragment.getCurrentTitle())
                return
            }
        }
    }

    private fun setupToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.apply {
            //            setDisplayHomeAsUpEnabled(true)
//            setHomeButtonEnabled(true)
//            setDisplayShowTitleEnabled(true)
        }
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            activity?.onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPress(): Boolean {
        val fragments = childFragmentManager.fragments
        if (fragments.size > 0) {
            for (fragment in fragments) {
                if (fragment is Test1HostFragment && viewPager.currentItem == TAB_1) {
                    return fragment.popBackStack()
                }
                if (fragment is Test2HostFragment && viewPager.currentItem == TAB_2) {
                    return fragment.popBackStack()
                }
                if (fragment is Test3HostFragment && viewPager.currentItem == TAB_3) {
                    return fragment.popBackStack()
                }
            }
        }
        return false
    }

}
