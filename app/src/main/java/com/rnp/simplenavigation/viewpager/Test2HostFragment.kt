package com.rnp.simplenavigation.viewpager

import android.os.Bundle
import com.rnp.simplenavigation.samplehost.ChildHostFragment


class Test2HostFragment : ChildHostFragment() {
    companion object {
        fun newInstance(tabIndex: Int): Test2HostFragment {

            val bundle = Bundle()
            bundle.putInt("TAB_INDEX", tabIndex)

            val fragment = Test2HostFragment()
            fragment.arguments = bundle

            return fragment
        }
    }
}
