package com.rnp.simplenavigation.viewpager

import android.os.Bundle
import com.rnp.simplenavigation.samplehost.ChildHostFragment


class Test3HostFragment : ChildHostFragment() {
    companion object {
        fun newInstance(tabIndex: Int): Test3HostFragment {

            val bundle = Bundle()
            bundle.putInt("TAB_INDEX", tabIndex)

            val fragment = Test3HostFragment()
            fragment.arguments = bundle

            return fragment
        }
    }
}
