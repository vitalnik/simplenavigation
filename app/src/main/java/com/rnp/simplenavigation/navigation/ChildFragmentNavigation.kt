package com.rnp.simplenavigation.navigation

interface ChildFragmentNavigation {

    fun popBackStack():Boolean

    fun updateTitle(title: String)

}
