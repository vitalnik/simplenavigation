package com.rnp.simplenavigation.navigation

interface BackPressConsumer {

    fun onBackPress():Boolean

}
