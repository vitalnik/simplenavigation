package com.rnp.simplenavigation.bottomnav1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rnp.simplenavigation.R
import com.rnp.simplenavigation.navigation.BackPressConsumer
import com.rnp.simplenavigation.navigation.ChildFragmentNavigation
import com.rnp.simplenavigation.viewpager.ViewPagerFragment


class Nav1HostFragment : Fragment(), BackPressConsumer {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_host_nav1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // loading first fragment in the host
        if (childFragmentManager.findFragmentById(R.id.fragmentHostNav1Container) == null) {
            childFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHostNav1Container, ViewPagerFragment())
                    .commit()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        fun newInstance(): Nav1HostFragment {
            return Nav1HostFragment()
        }
    }

    override fun onBackPress(): Boolean {
        val fragments = childFragmentManager.fragments
        if (fragments.size > 0) {
            for (fragment in fragments) {
                if (fragment is BackPressConsumer) {
                    if (fragment.onBackPress()) {
                        return true
                    }
                }
                if (fragment is ChildFragmentNavigation) {
                    return fragment.popBackStack()
                }
            }
        }
        return false
    }

}
