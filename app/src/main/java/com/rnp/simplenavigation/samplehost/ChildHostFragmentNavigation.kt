package com.rnp.simplenavigation.samplehost

import com.rnp.simplenavigation.navigation.ChildFragmentNavigation

interface ChildHostFragmentNavigation: ChildFragmentNavigation {

    fun createNewChild()

}
