package com.rnp.simplenavigation.samplehost

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rnp.simplenavigation.R
import kotlinx.android.synthetic.main.fragment_child.*

class ChildFragment : Fragment() {

    var navigation: ChildHostFragmentNavigation? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (parentFragment != null && parentFragment is ChildHostFragmentNavigation) {
            navigation = parentFragment as ChildHostFragmentNavigation?
        }
    }

    companion object {
        fun newInstance(tabIndex: Int, count: Int): ChildFragment {

            var bundle = Bundle()
            bundle.putInt("TAB_INDEX", tabIndex)
            bundle.putInt("COUNT", count)

            var fragment = ChildFragment()
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_child, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        testLabel.text = arguments?.getInt("COUNT").toString()

        var currentTabIndex = (arguments?.getInt("TAB_INDEX") ?: 0) + 1

        navigation?.updateTitle("Child Fragment: " + arguments?.getInt("COUNT").toString())

        newFragmentButton.setOnClickListener {
            navigation?.createNewChild()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            navigation?.popBackStack()
            return true
        }

        return super.onOptionsItemSelected(item)
    }


}
