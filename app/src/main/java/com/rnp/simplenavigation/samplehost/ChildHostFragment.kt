package com.rnp.simplenavigation.samplehost

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.rnp.simplenavigation.R


open class ChildHostFragment : Fragment(), ChildHostFragmentNavigation {

    private var fragmentCount = 0

    private var currentTitle = ""

    private var tabIndex: Int = 0

    override fun onSaveInstanceState(outState: Bundle) {

        outState.putInt("COUNT", fragmentCount)

        super.onSaveInstanceState(outState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_child_host, container, false)

        tabIndex = arguments?.getInt("TAB_INDEX") ?: 0

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            fragmentCount = savedInstanceState.getInt("COUNT")
        }

        if (childFragmentManager.findFragmentById(R.id.fragmentTestHostContainer) == null) {
            childFragmentManager.beginTransaction()
                    .replace(R.id.fragmentTestHostContainer, ChildFragment.newInstance(tabIndex, fragmentCount))
                    .commit()
        }

    }

    /**
     * Implemented from ChildHostFragmentNavigation
     */
    override fun createNewChild() {

        fragmentCount++

        val fragment = ChildFragment.newInstance(tabIndex, fragmentCount)

        val fragmentTransaction = childFragmentManager.beginTransaction()

        fragmentTransaction.replace(R.id.fragmentTestHostContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.setPrimaryNavigationFragment(fragment)

        fragmentTransaction.commit()
    }

    override fun popBackStack(): Boolean {

        fragmentCount--

        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack()
            return true
        }

        return false
    }

    fun getCurrentTitle(): String {
        return currentTitle
    }

    override fun updateTitle(title: String) {

        currentTitle = title

        (activity as AppCompatActivity).supportActionBar?.apply {

            setTitle(title)

            if (fragmentCount > 0) {
                setDisplayHomeAsUpEnabled(true)
                setHomeButtonEnabled(true)
            } else {
                setDisplayHomeAsUpEnabled(false)
                setHomeButtonEnabled(false)
            }
        }
    }

}
