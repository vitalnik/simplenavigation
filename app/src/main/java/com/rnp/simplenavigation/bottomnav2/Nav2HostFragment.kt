package com.rnp.simplenavigation.bottomnav2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.rnp.simplenavigation.R
import com.rnp.simplenavigation.navigation.ChildFragmentNavigation
import com.rnp.simplenavigation.samplehost.ChildHostFragment
import kotlinx.android.synthetic.main.fragment_nav1_viewpager.*


class Nav2HostFragment : Fragment(), Nav2HostFragmentNavigation {

    var topHomeNavigation: Nav2HostFragmentNavigation? = null

    companion object {
        fun newInstance(): Nav2HostFragment {
            return Nav2HostFragment()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (parentFragment != null && parentFragment is Nav2HostFragmentNavigation) {
            topHomeNavigation = parentFragment as Nav2HostFragmentNavigation?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_host_nav2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setupToolbar()

        // loading first fragment in the host
        if (childFragmentManager.findFragmentById(R.id.fragmentHostNav2Container) == null) {
            childFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHostNav2Container, ChildHostFragment())
                    .commit()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.apply {
            //            setDisplayHomeAsUpEnabled(true)
//            setHomeButtonEnabled(true)
//            setDisplayShowTitleEnabled(true)
        }
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            activity?.onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPress(): Boolean {
        val fragments = childFragmentManager.fragments
        if (fragments.size > 0) {
            for (fragment in fragments) {
                if (fragment is ChildFragmentNavigation) {
                    return fragment.popBackStack()
                }
            }
        }
        return false
    }
}
